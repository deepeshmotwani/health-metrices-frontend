const express = require('express');
const db = require('./config/db')
const cors = require('cors')

const app = express();
const  PORT = 5000;
app.use(cors());
app.use(express.json())

// Route to get all posts
app.post("/get_data",async (req,res)=>{
    
    db.query("SELECT * FROM `health-ping` WHERE `instance-id` = '"+req.body['instance-selected']+"' AND `app-name` = '"+req.body['app-name']+"' AND `timestamp` >= '"+req.body['dateStart']+"' AND `timestamp` <= '"+req.body['dateEnd']+"'", (err,result)=>{
        if(err) {
        console.log(err)
        } 
        // console.dir(req.body)
        res.send(result);
    });   
    
    });

// Route to get all posts
app.post("/get_app_name",async (req,res)=>{
    
    db.query("SELECT distinct `app-name` FROM `health-ping`", (err,result)=>{
        if(err) {
        console.log(err)
        } 
        // console.dir(req.body)
        res.send(result);
    });   
    
    });
        
app.post("/get_instance_list",async (req,res)=>{
    db.query('SELECT distinct `instance-id` FROM `health-ping` where `app-name` = "'+req.body["app-name"]+'"', (err,result)=>{
        if(err) {
        console.log(err)
        } 
        // console.dir(req.body)
        res.send(result);
    });   
    
    });

    app.post("/getmindate",async (req,res)=>{
    
        db.query('SELECT MAX(`timestamp`) as MinDate FROM `health-ping` where `app-name` = "'+req.body["app-name"]+'" and `instance-id` = "'+req.body["instance-selected"]+'"', (err,result)=>{
            if(err) {
            console.log(err)
            } 
            res.send(result);
        });   
        
        });

app.post("/get_all_data",async (req,res)=>{
    
        db.query("select `app-name`,sum(`system-cpu`)/count(`instance-id`) as 'system-cpu',sum(`system-ram`)/count(`instance-id`) as 'system-ram',sum(`system-swap`)/count(`instance-id`) as 'system-swap',sum(`system-disk`)/count(`instance-id`) as 'system-disk', FROM_UNIXTIME(UNIX_TIMESTAMP(timestamp),'%Y-%m-%d %H:%i') as customtimestamp from `health-ping` WHERE `app-name`='"+req.body["app-name"]+"' GROUP BY customtimestamp;", (err,result)=>{
            if(err) {
            console.log(err)
            } 
            res.send(result);
        });   
        
        });

app.post("/get_avg_data",async (req,res)=>{
    var instance_type = ""
    db.query("select `instance-type`  from `health-ping` WHERE `app-name`='"+req.body["app-name"]+"' LIMIT 1;", (err,result)=>{
        if(err) {
            console.log(err)
            } 
            instance_type = result[0]['instance-type']
        });
    db.query("select `app-name`,sum(`process-name-count`)/count(`instance-id`) as 'process-name-count',sum(`process-all-count`)/count(`instance-id`) as `process-all-count`,sum(`system-cpu`)/count(`instance-id`) as 'system-cpu',sum(`system-ram`)/count(`instance-id`) as 'system-ram',sum(`system-swap`)/count(`instance-id`) as 'system-swap',sum(`system-disk`)/count(`instance-id`) as 'system-disk' from `health-ping` WHERE `app-name`='"+req.body["app-name"]+"';", (err,result)=>{
        if(err) {
        console.log(err)
        } 
        result[0]['instance-type']=instance_type
        res.send(result);
    });   
    
    });

app.listen(PORT, ()=>{
    console.log(`Server is running on ${PORT}`)
})