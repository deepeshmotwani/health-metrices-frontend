import './App.css';
import NavBar from './components/NavBar';
import {BrowserRouter as Router,Switch,Route,Redirect} from 'react-router-dom';
import Home from './pages/home';
import Instanceprices from './pages/instanceprices';
import InstanceSpecs from './pages/instancespecs';
import HealthMetrices from './pages/healthmetrices';
import PriceCalculator from './pages/pricecalculator';

function App() {
return (
    <>
      <Router>
      <NavBar />
      <Switch>
      <Route exact path="/">
        <Redirect to="/home" />
      </Route>
      <Route path="/home" component={Home}/>
      <Route path="/instanceprices" component={Instanceprices}/>
      <Route path="/healthmetrices" component={HealthMetrices}/>
      <Route path="/instancespecs" component={InstanceSpecs}/>
      <Route path="/pricecalculator" component={PriceCalculator}/>
      </Switch>
      </Router>
      
    </>
);
}

export default App;
