import React from 'react'
import homebg from '../img/home.jpg'
function home() {

    return (
        <>
        <div className="home_img" style={{"background-image":`url(${homebg})`}}>
            <div className="home">
            <div className='home_quote'>
                Don't optimize for conversions, optimize for revenue
            </div>
            <div className='home_author'>
                - Neil Patel
            </div>
            <div className="home_para">
                This api lets you choose what's the best AWS instance for you, by calculating the usage of your current AWS instances. 
                You can compare various General Purpose instances, know their specifications and prices, and decide what's best.
            </div>

            </div>


        </div>
        {/* <div className="horizontal_bar"/> */}
        </>
    )
}

export default home
