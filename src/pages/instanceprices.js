import React from 'react';
import instancePrices from '../data/instance_prices.json';
function instanceprices() {

    
    const sortFunct = (instancePriceList,sortFactor) => {
        instancePriceList.sort(function(a, b) {
            var keyA = a[sortFactor],
            keyB = b[sortFactor];
            // Compare the 2 dates
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        });
        return instancePriceList
    }
    var instancePriceList = sortFunct(instancePrices,"Instance Type")
    
    const handleDragStart = (e) => {
        console.log(e)
    }
    const listinstancelist = instancePriceList.map((instancePriceList)=>{  

        return  <div className="ondemandpricebox" key={instancePriceList["Instance Type"]} value={instancePriceList["Instance Type"]} onDragCapture={handleDragStart}>
                            {instancePriceList["Instance Type"]} {instancePriceList["price"]}
                    </div>; 
    }); 

    return (
        <>
        <div className='instanceprices' id="heading">
            <h1>Instance Prices</h1>
        </div>
        <div className="horizontal_bar"/>
        <div className='instanceprices'>
            For on Demand Pricing you can visit : <a href="https://aws.amazon.com/ec2/pricing/on-demand/" style={{"margin-left":"20px"}} target="_blank"> On Demand Pricing</a>
        </div>        
        <div className='instanceprices'>
            For on Spot Pricing you can visit : <a href="https://aws.amazon.com/ec2/spot/pricing/" style={{"margin-left":"20px"}} target="_blank"> On Spot Pricing</a>
        </div>        
        {/* <div className='instanceprices' id= "ondemandcontainer">
            {listinstancelist}
        </div> */}

        </>
    )
}

export default instanceprices
