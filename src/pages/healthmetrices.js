import React,{useEffect,useState} from 'react'; 
import instancedata from '../data/instance_data.json' 
import DatePicker from 'react-date-picker'; 
import charts from '../components/charts'; 
import donut from '../components/donut'; 
import axios from 'axios'; 
import ReactApexChart  from 'react-apexcharts'; 
import Popup from '../components/popup';   
function Healthmetrices() {     
    let [instanceSelected, setInstanceSelectedValue] = useState('All');     
    const [isOpen, setIsOpen] = useState(false);     
    let [dateStartSelected, setStartDateSelected] = useState(new Date(2021,8,3));     
    let [dateEndSelected, setEndDateSelected] = useState(new Date());     
    const [systemmetrics, setSystemMetrics] = useState(["cpu","ram","swap","disk"]);     
    const [granularityVar, setGranularity] = useState(1);     
    const [dataResponse, setDataResponse] = useState({});     
    const [timestampColumnName, setTimestampColumnName] = useState("timestamp");     
    const [appName, setAppNames] = useState([]);     
    const [instanceList, setinstanceLists] = useState([]);     
    let [appNameValueSelected, setAppNameValue] = useState("test");     
    let [no_of_total_process, setTotalProcess] = useState(100);      
    const [instanceType,setInstanceType] = useState("");     
    const [instancePublicIP,setInstancePublicIP] = useState("");     
    const [instancePrivateIP,setInstancePrivateIP] = useState("");     
    const [instanceAWSRegion,setInstanceAWSRegion] = useState("");          
    
    //////////// GET APP NAMES           
    const appName_list = async()=> {         
        try{ 
            const resp = await axios.post('/get_app_name'); 
            if (resp.status === 200) {  
                return resp.data; 
            } 

            return false;         
        } 
        catch (err) { 
            console.error(err) 
            return false;         
        }     
    }; 
 
    const instance_list = async(app_name)=> {         
        try{         
            const resp = await axios.post('/get_instance_list',{'app-name':app_name});         
            if (resp.status === 200) { 
                return resp.data; 
            }
                return false;          
        } 
        catch (err) 
        {            
            console.error(err)            
            return false;          
        }     
    };         
    const listChanged = async(e)=> {         
        if (e.instanceName==="All"){ 
            setTimestampColumnName("customtimestamp") 
            const resp = await axios.post('/get_all_data',{"app-name":e.appName}) 
            var startDate = Date.parse(resp.data[resp.data.length-1]["customtimestamp"])-86400000 
            if (e.dateStart){ 
                startDate = e.dateStart.getTime() 
            } 
            var endDate = (new Date()).getTime() 
            if (e.dateEnd){ 
                endDate = e.dateEnd.getTime() 
            } 
            var resp_data = [] 
            for(var i=0;i<=resp.data.length-1;i++){ 
                if(Date.parse(resp.data[i]["customtimestamp"])>=startDate && Date.parse(resp.data[i]["customtimestamp"])<=endDate) 
                        { 
                            resp_data.push(resp.data[i]) 
                        } 
                    
                        } 
                    setDataResponse(resp_data); 
                    setStartDateSelected(new Date(Date.parse(resp.data[resp.data.length-1]["customtimestamp"])-86400000)) 
                    setInstanceType("All") 
                    setInstancePublicIP("All") 
                    setInstancePrivateIP("All") 
                    setInstanceAWSRegion("All") 
                        setGranularity(1)         
            }         
                else{ 
                setTimestampColumnName("timestamp") 
                if (e.appName){ 
                    appNameValueSelected = e.appName 
                } 
                if (e.instanceName){ 
                    instanceSelected = e.instanceName 
                } 
                if (e.dateStart){ 
                    dateStartSelected = e.dateStart 
                } 
                if (e.dateEnd){ 
                    dateEndSelected = e.dateEnd 
                } 
    const resp = await axios.post('/get_data',{"app-name":appNameValueSelected,"instance-selected":instanceSelected,"dateStart":dateStartSelected,"dateEnd":dateEndSelected}) 
    setDataResponse(resp.data); 
    try{ 
        setInstanceType(resp.data[0]['instance-type']) 
        setInstancePublicIP(resp.data[0]['instance-public-ip']) 
        setInstancePrivateIP(resp.data[0]['instance-private-ip']) 
        setInstanceAWSRegion(resp.data[0]['instance-aws-region']) 
    } 
    catch{ 
        console.log("mishappening") 
    } 
            }       
    };    

    const getMinDate = async(e)=> {         
        if (e.appName){ 
            appNameValueSelected = e.appName         
        }         
        if (e.instanceName){ 
            instanceSelected = e.instanceName         
        }           
        const resp = await axios.post('/getmindate',{"app-name":appNameValueSelected,"instance-selected":instanceSelected})         
        return resp.data       
        }; 

    useEffect(()=>{         
        appName_list("").then((value)=> { 
            setAppNames(value); 
            setAppNameValue(value[0]['app-name']) 
            instance_list(value[0]['app-name']).then((instance_list)=> { 
            setinstanceLists(instance_list) 
            setInstanceSelectedValue("All") 
            const e = {appName:value[0]['app-name'],instanceName:"All"} 
            // getMinDate(e).then((minDate)=> { 
            //     setStartDateSelected(new Date(Date.parse(minDate[0]["MinDate"])-86400000)) 
            //     const g = {...e,dateStart:new Date(Date.parse(minDate[0]["MinDate"])-86400000)} 
                listChanged(e) 
            // }) 
            } 
            ); 
        }) 
    }, [])  

    const listappname = appName.map((appName)=>{            
        return <option id="togglelist_item" key={appName["app-name"]} value={appName["app-name"]}> 
                {appName["app-name"]} 
        </option>;        
        }); 

    const onClickPrices = ()=> {         
        togglePopup();         
        show_prices(dataResponse,dataset);     
        }   

    const togglePopup = ()=> {         
        setIsOpen(!isOpen); 
    }     

    const handleChangeAppName = (e)=> {         
        setAppNameValue(e.target.value);           
        instance_list(e.target.value).then((instance_list)=> { 
            setinstanceLists(instance_list) 
            setInstanceSelectedValue(instance_list[0]['instance-id']); 
            let f = {appName:e.target.value,instanceName:"All"} 
            // getMinDate(f).then((minDate)=> { 
            //     setStartDateSelected(new Date(Date.parse(minDate[0]["MinDate"])-86400000)) 
            //     const g = {...f,dateStart:new Date(Date.parse(minDate[0]["MinDate"])-86400000)} 
                listChanged(f); 
            // }) 
        });     
    }      
    const handleAgentChange = (e)=> {         
        if (e.target.value != ""){ 
            setTotalProcess(e.target.value) 
        }     
    }     
     
    const handlePriceBoxClicked = (e)=> { 
        }      
    
    //////////////////////////////////       ///////////////////////////////// INSTANCE ID LIST      
    const handleChangeSystemMetrics = (e)=> {         
        let sys_met_list = ["cpu","ram","swap","disk"]         
        if (e.target.value === "All")         
        {            

        }         
        else { 
            sys_met_list = [] 
            sys_met_list.push(e.target.value.toLowerCase()) 
            }         
        setSystemMetrics(sys_met_list);       
    }     
     
    const listinstancelist = instanceList.map((instanceList)=>{            
        return <option id="togglelist_item" key={instanceList["instance-id"]} value={instanceList["instance-id"]}> 
            {instanceList["instance-id"]} 
            </option>;       
        });           
    
    const handleChangeInstanceId = (e)=> {         
        if (e.target.value !== "All"){ 
            setInstanceSelectedValue(e.target.value); 
            const f = {appName:appNameValueSelected,instanceName:e.target.value} 
            getMinDate(f).then((minDate)=> { 
                setStartDateSelected(new Date(Date.parse(minDate[0]["MinDate"])-86400000)) 
                const g = {instanceName:e.target.value,dateStart:new Date(Date.parse(minDate[0]["MinDate"])-86400000)} 
                listChanged(g); 
                    })         
            }         
        if (e.target.value === "All"){ 
            setInstanceSelectedValue(e.target.value); 
            const f = {appName:appNameValueSelected,instanceName:"All"} 
            listChanged(f); 
            console.log(e.target.value) 
                }      
    }      

    const handleStartDateChanged = (e)=> {         
        if (instanceSelected !== "All"){ 
            setStartDateSelected(e); 
            const g = {dateStart:new Date(Date.parse(e)+37800000)} 
            listChanged(g); 
        } 
        else{ 
            setStartDateSelected(e); 
            const g = {appName:appNameValueSelected,instanceName:"All",dateStart:new Date(Date.parse(e)+37800000)} 
            listChanged(g); 
        }     
    }      
    const handleEndDateChanged = (e)=> {         
        if (instanceSelected !== "All"){ 
            setEndDateSelected(e); 
            const g = {dateEnd:new Date(Date.parse(e)+2*19800000)} 
            listChanged(g);           
        }         
        else{ 
                setEndDateSelected(e); 
        const g = {appName:appNameValueSelected,instanceName:"All",dateEnd:new Date(Date.parse(e)+2*19800000)} 
        listChanged(g);           
        }    
    }      
    
    const handleChangeGranularity = (e)=> { 
        if (e.target.value === "5 min"){ 
                setGranularity(1);         
            }         if (e.target.value === "10 min")         { 
                setGranularity(2);
        }
        if (e.target.value === "30 min")
        { 
            setGranularity(6);
        }
        if (e.target.value === "1 hr")
        { 
            setGranularity(10);
        }
        if (e.target.value === "6 hr")
        { 
            setGranularity(60);
        }
        if (e.target.value === "12 hr")
        { 
            setGranularity(120);         
        } 
    };      // ###### GRAPH #################     
    const unique_set = {'time': [],'cpu':[],'ram':[],"swap":[],disk:[]}     
    let count = 0     
    let cpu_gran = 0     
    let ram_gran = 0     
    let disk_gran = 0     
    let swap_gran = 0     
    const healthdata = dataResponse      
    for (const [key, value] of Object.entries(healthdata)) {         
        count += 1;         
        cpu_gran += value["system-cpu"] 
        ram_gran += value["system-ram"] 
        disk_gran += value["system-disk"] 
        swap_gran += value["system-swap"] 
        if (count===granularityVar){ 
            unique_set.time.push(value[timestampColumnName]) 
            unique_set.cpu.push(parseFloat(cpu_gran/granularityVar).toFixed(2)) 
            unique_set.ram.push(parseFloat(ram_gran/granularityVar).toFixed(2)) 
            unique_set.disk.push(parseFloat(disk_gran/granularityVar).toFixed(2)) 
            unique_set.swap.push(parseFloat(swap_gran/granularityVar).toFixed(2)) 
            cpu_gran=0 
            ram_gran=0 
            disk_gran=0 
            swap_gran=0 
            count = 0 
        } 
    };          
    let dataset = {}     
    if (unique_set.time.length>0){         
        for (const [key,value] of Object.entries(unique_set)) 
        { 
    if (key !== "time") 
    { 
        const sum = value.reduce((a, b)=> parseFloat(a) + parseFloat(b), 0); 
        const avg = parseFloat((sum / value.length)).toFixed(2) || 0; 
        dataset[key] = avg 
    }          }     }     
const show_chart = charts(unique_set,systemmetrics,dateStartSelected);          
const [instances_specs,setInstanceSpecs] = useState([]);      
const show_prices = (dataResponse,dataset)=> {         
    let instances_specs = []         
        for (const item in Object.entries(instancedata)){ 
        if (instancedata[item]["instance_size_name"] === dataResponse[0]["instance-type"]) 
        { 
            const current_instance_ram = instancedata[item]["RAM"] 
            const current_instance_cpu = instancedata[item]["vCPU"] 
            const no_of_agents_running = dataResponse[0]["process-name-count"] 
            const effective_ram = current_instance_ram/no_of_agents_running 
            const effective_cpu = current_instance_cpu/no_of_agents_running 
            for(const item in Object.entries(instancedata)){ 
        
            let instance_ram = instancedata[item]["RAM"]; 
                let instance_cpu = instancedata[item]["vCPU"]; 
                let no_of_agent_possible = parseInt(Math.min(instance_ram/effective_ram,instance_cpu/effective_cpu)) 
                let no_of_instance_required = parseInt(no_of_total_process/no_of_agent_possible) || 1; 
                if (no_of_agent_possible!=0){ 
                    instances_specs.push({
                        name:instancedata[item]["instance_size_name"], 
                        RAM:instance_ram, 
                        vCPU:instance_cpu, 
                        Total_Agent:no_of_agent_possible, 
                        Total_Instance:parseInt(no_of_instance_required), 
                        Price_hour:parseFloat(no_of_instance_required*instancedata[item]["Average/Hour"]).toFixed(2), 
                        Price_day:parseFloat(no_of_instance_required*instancedata[item]["Average/day"]).toFixed(2), 
                    }) 
                } 
            } 
        }         
    }         
    instances_specs.sort(function(a,b) { 
        return a.Price_day - b.Price_day         
    });          
    setInstanceSpecs(instances_specs)         
    return instances_specs     
}     
const check_current_instance = (name)=> {         
    if (dataResponse[0]["instance-type"]===name)         
    { 
        return <label style={{"font-size":"15px"}}>(Current)</label>         
    }     
}          
const showPriceDIV = instances_specs.map((instances_specs)=>{ 
   return <div className="popup_box" key={instances_specs.name}> 
            <div className="Pricing_box" onClick={handlePriceBoxClicked}> 
 
   <label id="pricing_label"s> 
 
   {instances_specs.name} 
 
   {check_current_instance(instances_specs.name)} 
 
   </label> 
 
   <br/> 
 
   <div className="specs_ram_cpu"> 
 
       <label id="specs_label"> 
 
 
RAM : {instances_specs.RAM}gb 
 
         </label> 
 
           <br/> 
 
         <label id="specs_label"> 
 
 
CPU : {instances_specs.vCPU}vCPU 
 
         </label> 
 
       </div> 
 
   <br/> 
 
   <label id="specs_label"> 
 
         Total Agents : {instances_specs.Total_Agent} 
 
     </label> 
 
     <br/> 
 
    <label id="specs_label"> 
 
         Total Instance required : {instances_specs.Total_Instance} 
 
     </label> 
 
     <br/> 
 
   <br/> 
 
   <br/> 
 
   <br/> 
 
   <br/> 
 
   <br/> 
 
    <label id="specs_label"> 
 
         $ 
 
   </label> 
 
     <label id="hour_label"> 
 
         {instances_specs.Price_hour} 
 
   </label> 
 
      <label id="specs_label"> 
 
         /hour 
 
   </label> 
 
 
 
        <br/> 
 
     <label id="specs_label"> 
 
         $ 
 
   </label> 
 
     <label id="day_label"> 
 
         {instances_specs.Price_day} 
 
   </label> 
 
      <label id="specs_label"> 
 
         /day 
 
   </label> 
 
      <br/> 
            </div> 
        </div>;            }); 
 // ################################## 
 
     return (         <> 
<div className='healthmetrices' id="heading"> 
    <h1>Health Metrices</h1> 
</div> 
<div className="horizontal_bar"/> 
 
    {/* <div> 
        {donut([44])} 
    </div> 
    <div> 
        {donut([44])} 
    </div> */} 
 <div className='healthmetrices'> 
 
    <label id="label"> 
        App-Name 
     </label> 
 
 
       <select id="togglelist" onChange={handleChangeAppName}> 
            {listappname} 
        </select> 
     <label id="label"> 
        Instance-id 
    </label> 
 
 
   <select id="togglelist" onChange={handleChangeInstanceId}> 
            <option id="togglelist_item"> 
 
   All 
            </option> 
 
 
    {listinstancelist} 
        </select> 
 
 
    <label id="label"> 
        Start Date 
    </label> 
        <DatePicker className="datepicker" 
 
       onChange={handleStartDateChanged} 
 
       value={dateStartSelected} 
 
       clearIcon={null} 
            /> 
         <label id="label"> 
        End Date 
    </label> 
        <DatePicker className="datepicker" 
 
       onChange={handleEndDateChanged} 
 
       value={dateEndSelected} 
 
       clearIcon={null} 
            /> 
 
    </div> 
         <div className='healthmetrices'> 
 <label id="label"> 
    System metrics 
</label> 
 
    <select id="togglelist" onChange={handleChangeSystemMetrics}> 
        <option id="togglelist_item">All</option> 
        <option id="togglelist_item">RAM</option> 
        <option id="togglelist_item">CPU</option> 
        <option id="togglelist_item">DISK</option> 
        <option id="togglelist_item">SWAP</option> 
    </select> 
          <label id="label"> 
    Granularity 
</label> 
 
    <select id="togglelist" onChange={handleChangeGranularity}> 
        <option id="togglelist_item">5 min</option> 
        <option id="togglelist_item">10 min</option> 
        <option id="togglelist_item">30 min</option> 
        <option id="togglelist_item">1 hr</option> 
        <option id="togglelist_item">6 hr</option> 
        <option id="togglelist_item">12 hr</option> 
    </select> 
 
    <label id="label"> 
    See Estimate Prices for 
 </label> 
 
<select id="togglelist_prices" onChange={handleAgentChange}> 
        <option id="togglelist_item">100</option> 
        <option id="togglelist_item">500</option> 
        <option id="togglelist_item">1000</option> 
        <option id="togglelist_item">2000</option> 
        <option id="togglelist_item">5000</option> 
        <option id="togglelist_item">10000</option> 
        <option id="togglelist_item">20000</option> 
</select>          <input  className="price_button" 
    type="button" 
    value="Show Estimated Prices" 
    onClick={onClickPrices} 
    />      {isOpen && <Popup       content={<> 
<label id="estimate_price_label"> 
    Estimated Prices for : {no_of_total_process} Agents 
</label>         <div className="popup_container"> 
{showPriceDIV}         </div>        </>}       handleClose={togglePopup}     />}         </div>          <div id="charts"> 
{show_chart}         </div>         { true &&         <div className="donut"> 
<div className="list_donut"> 
    <p className="instancedetails">Instance Name: {instanceType}</p> 
    <p className="instancedetails">Instance Public IP: {instancePublicIP}</p> 
    <p className="instancedetails">Instance Private IP: {instancePrivateIP}</p> 
    <p className="instancedetails">Instance AWS Region: {instanceAWSRegion}</p> 
</div> 
<div className="list_donut"> 
    {donut({name:"cpu",value:[dataset.cpu],color:"#66DA26"})} 
</div> 
<div className="list_donut"> 
    {donut({name:"ram",value:[dataset.ram],color:"#546E7A"})} 
</div> 
<div className="list_donut"> 
    {donut({name:"disk",value:[dataset.disk],color:"#2E93fA"})} 
</div> 
<div className="list_donut"> 
    {donut({name:"swap",value:[dataset.swap],color:"#E91E63"})} 
</div>          </div>         }            </> 
 ) };  export default Healthmetrices 