import React,{useEffect,useState} from 'react';
import instancedata from '../data/instance_data.json'
import { DataGrid } from '@mui/x-data-grid';
import axios from 'axios';
import Popup from '../components/popup';


function PriceCalculator() {
    let [instanceSelected, setInstanceSelectedValue] = useState('i-0132aaa42e450be5b');
    const [isOpen, setIsOpen] = useState(true);
    const [dataResponse, setDataResponse] = useState({});
    const [appName, setAppNames] = useState([]);
    const [instanceList, setinstanceLists] = useState([]);
    let [appNameValueSelected, setAppNameValue] = useState("api_server");
    let [no_of_total_process, setTotalProcess] = useState(100);

    //////////// GET APP NAMES 
    
    const appName_list = async() => {
        try{
            const resp = await axios.post('/get_app_name');
            if (resp.status === 200) { 
                return resp.data;
            }
            return false;
        } catch (err) {
            console.error(err)
            return false;
        }
    };
        
    const instance_list = async(app_name) => {
        try{
        const resp = await axios.post('/get_instance_list',{'app-name':app_name});
        if (resp.status === 200) { 
            return resp.data;
            }
            return false;
         } catch (err) {
           console.error(err)
           return false;
         }
    };
    
    const listChanged = async(e) => {
        const resp = await axios.post('/get_avg_data',{"app-name":appNameValueSelected,"instance-selected":instanceSelected})
        setDataResponse(resp.data);
        show_prices(resp.data);

      };

    useEffect(()=>{
        appName_list("").then((value) => {
                setAppNames(value);
                setAppNameValue(value[0]['app-name'])
                instance_list(value[0]['app-name']).then((instance_list) => {
                    setinstanceLists(instance_list)
                    setInstanceSelectedValue(instance_list[0]['instance-id'])
                    listChanged();
                    });
                })
            }, [])
    
    const listappname = appName.map((appName)=>{   
        return <option id="togglelist_item" key={appName["app-name"]} value={appName["app-name"]}>
                        {appName["app-name"]}
                </option>;   
    });   
    

    const onClickPrices = () => {
        listChanged();
        show_prices(dataResponse);
    }
    const togglePopup = () => {
        setIsOpen(!isOpen);
        }


    const handleChangeAppName = (e) => {
        setAppNameValue(e.target.value);  
        instance_list(e.target.value).then((instance_list) => {
            setinstanceLists(instance_list)
            setInstanceSelectedValue(instance_list[0]['instance-id']); 
            });
    }

    const handleAgentChange = (e) => {
        if (e.target.value != ""){
            setTotalProcess(e.target.value)        
        }
    }

    const handlePriceBoxClicked = (e) => {
        
    }

    const listinstancelist = instanceList.map((instanceList)=>{   
        return <option id="togglelist_item" key={instanceList["instance-id"]} value={instanceList["instance-id"]}>
                        {instanceList["instance-id"]}
                </option>;   
    }); 
    
    const handleChangeInstanceId = (e) => {
        setInstanceSelectedValue(e.target.value); 
    }


    const columns = [
        { field: 'id', headerName: 'ID', width: 50 },
        { field: 'instanceName', headerName: 'Instance Name', width: 180 },
        { field: 'ram', headerName: 'Ram', width: 70 },
        { field: 'vcpu', headerName: 'vCPU', width: 70 },
        { field: 'agents', headerName: 'Agents/Instance',type:'number', width: 150 },
        { field: 'instances', headerName: 'Instances Required',type:'number', width: 150 },
        { field: 'pricePerHour', headerName: 'Price/Hour',type:'number', width: 130 },
        { field: 'pricePerDay', headerName: 'Price/Day',type:'number', width: 130 },
      ];
    
    // ###### GRAPH #################
  
    const [instances_specs,setInstanceSpecs] = useState([]);

    const show_prices = (dataResponse) => {
        let instances_specs = []
        var id_ = 1
        for (const item in Object.entries(instancedata)){
            if (instancedata[item]["instance_size_name"] === dataResponse[0]["instance-type"])
            {
                const current_instance_ram = instancedata[item]["RAM"]
                const current_instance_cpu = instancedata[item]["vCPU"]
                const no_of_agents_running = dataResponse[0]["process-name-count"]
                const effective_ram = current_instance_ram/no_of_agents_running
                const effective_cpu = current_instance_cpu/no_of_agents_running
                for(const item in Object.entries(instancedata)){
        
                    let instance_ram = instancedata[item]["RAM"];
                    let instance_cpu = instancedata[item]["vCPU"];
                    let no_of_agent_possible = parseInt(Math.min(instance_ram/effective_ram,instance_cpu/effective_cpu))
                    let no_of_instance_required = parseInt(no_of_total_process/no_of_agent_possible) || 1;
                    if (no_of_agent_possible!=0){
                        instances_specs.push({
                                            id : id_,
                                            instanceName:dataResponse[0]["instance-type"]===instancedata[item]["instance_size_name"] ? instancedata[item]["instance_size_name"]+"(Current)" : instancedata[item]["instance_size_name"] ,
                                            ram:instance_ram,
                                            vcpu:instance_cpu,
                                            agents:no_of_agent_possible,
                                            instances:parseInt(no_of_instance_required),
                                            pricePerHour:parseFloat(no_of_instance_required*instancedata[item]["Average/Hour"]).toFixed(2),
                                            pricePerDay:parseFloat(no_of_instance_required*instancedata[item]["Average/day"]).toFixed(2),
                                            })
                                        }
                        id_++;
                }
            }
        }

        instances_specs.sort(function(a,b) {
            return a.pricePerDay - b.pricePerDay
        });

        setInstanceSpecs(instances_specs)
        return instances_specs
    }

    console.log(instances_specs)
    const check_current_instance = (name) => {
        if (dataResponse[0]["instance-type"]===name)
        {
            return <label style={{"font-size":"15px"}}>(Current)</label>
        }
    }
    
    return (
        <>
            <div className='healthmetrices' id="heading">
                <h1>Price Calculator</h1>
            </div>
            <div className="horizontal_bar"/>

            <div className='healthmetrices'>
            
                <label id="label">
                    App-Name 
                </label>
                        
                    <select id="togglelist" onChange={handleChangeAppName}>
                        {listappname}
                    </select>

                <label id="label">
                    Instance-id
                </label>
                    
                    <select id="togglelist" onChange={handleChangeInstanceId}>
                    {listinstancelist}
                    </select>

            </div>    
        
        <div className='healthmetrices'>
            <label id="label">
                Total no of processes you want to run 
            </label>
            
            <select id="togglelist_prices" onChange={handleAgentChange}>
                    <option id="togglelist_item">100</option>
                    <option id="togglelist_item">500</option>
                    <option id="togglelist_item">1000</option>
                    <option id="togglelist_item">2000</option>
                    <option id="togglelist_item">5000</option>
                    <option id="togglelist_item">10000</option>
                    <option id="togglelist_item">20000</option>
            </select>


        <input  className="price_button"
                type="button"
                value="Show Estimated Prices"
                onClick={onClickPrices}
                />
        </div>

    <div className='healthmetrices'>

     {isOpen && <DataGrid
        rows={instances_specs}
        columns={columns}
        pageSize={20}
        rowsPerPageOptions={[20]}
        checkboxSelection
      />
      }
        </div>

        </>
        
    )
};

export default PriceCalculator
