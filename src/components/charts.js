import React from 'react'
import ReactApexChart  from 'react-apexcharts';

function charts(unique_set,systemmetrics,dateStartSelected) {
  let color = [];

    let series = []
    for (var val in systemmetrics) {
      if (systemmetrics[val] == "cpu"){
        color.push("#66DA26")
      }
      if (systemmetrics[val] == "ram"){
        color.push("#546E7A")
      }
      if (systemmetrics[val] == "swap"){
        color.push("#E91E63")
      }
      if (systemmetrics[val] == "disk"){
        color.push("#2E93fA")
      }
      series.push({data: unique_set[systemmetrics[val]],name:systemmetrics[val]})
  } 

    const state = {
    
        series: series,
        options: {
          legend:{
            show:false
        },
          colors: color,
          chart: {
            id: 'area-datetime',
            type: 'area',
            height: 350,
            
            // zoom: {
            //   autoScaleYaxis: true
            // },
            animations: {
                enabled: true,
                easing: 'easein',
                speed: 800,
                animateGradually: {
                    enabled: true,
                    delay: 1000
                },
                dynamicAnimation: {
                    enabled: true,
                    speed: 350
                }
            },

            toolbar: {
              show: false,
              offsetX: 0,
              offsetY: 0,
              tools: {
                download: true,
                selection: true,
                zoom: true,
                zoomin: true,
                zoomout: true,
                pan: true,
                reset: true | '<img src="/static/icons/reset.png" width="20">',
                customIcons: []
              },},
            // zoom: {
            //     enabled: true,
            //     type: 'x',
            //     resetIcon: {
            //         offsetX: -10,
            //         offsetY: 0,
            //         fillColor: '#fff',
            //         strokeColor: '#37474F'
            //     },
            //     selection: {
            //         background: '#90CAF9',
            //         border: '#0D47A1'
            //     }    
            // }
          },
          
          stroke:{
            width:2,
            curve: 'straight',
          },
          dataLabels: {
            enabled: false
          },
          xaxis: {
            type: 'datetime',
            categories:unique_set.time
          },
          tooltip: {
            x: {
              min: "22 08 1997 12:16",
              format: 'dd MMM yyyy HH:mm'
            }
          },
          fill: {
            type: 'gradient',
            gradient: {
              shadeIntensity: 1,
              opacityFrom: 0,
              opacityTo: 0.5,
              stops: [0, 100]
            }
          },
        },
        
      
      };

    return (
        <>
            <div>
            <ReactApexChart className="charts" options={state.options} series={state.series} type="area" height={300} width={1150} />
            </div>
        </>
    )
}

export default charts
