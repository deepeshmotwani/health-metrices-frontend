import React,{useState} from 'react';
import {Link} from 'react-router-dom';
import {SidebarData} from './SidebarData'
import './NavBar.css'

function Navbar(){
    const [sidebar,setSidebar] = useState(false)

    const showSidebar = () => setSidebar(!sidebar)

    return(
        <>
            {/* <IconContext.Provider value={{color: '#fff'}}> */}
            <div className="navbar">
                {/* <Link to="#" className='menu-bars'>
                    <FaIcons.FaBars onClick={showSidebar}/>
                </Link>  */}
            </div>
            <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
                <ul className='nav-menu-items'>
                    <li className="navbar-toggle">
                        {/* <Link to="#" className="menu-bars">
                            <AiIcons.AiOutlineClose/>
                        </Link> */}
                    </li>
                    {SidebarData.map((item,index) => {
                        return (
                            <li key={index} className='nav-text'>
                                <Link to={item.link}>
                                    {item.icon}
                                    <span>{item.title}</span>
                                </Link>
                            </li>
                        )
                    })}
                </ul>
            </nav>
            {/* </IconContext.Provider> */}
        </>

    )
}

export default Navbar;