import React from 'react'
import HomeIcon from '@material-ui/icons/Home';
import TimelineIcon from '@material-ui/icons/Timeline';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import DescriptionIcon from '@material-ui/icons/Description';
import FlightTakeoffIcon from '@material-ui/icons/FlightTakeoff';

export const SidebarData =  [
        {
            title :  "Home",
            icon  : <HomeIcon />,
            link  : "/home",
        },
        {
            title :  "Health Metrices",
            icon  : <TimelineIcon />,
            link  : "/healthmetrices",
        },
        {
            title :  "Instance Prices",
            icon  : <AttachMoneyIcon />,
            link  : "/instanceprices",
        },
        {
            title :  "Instance Specifications",
            icon  : <DescriptionIcon />,
            link  : "/instancespecs",
        },
        {
            title :  "Price Calculator",
            icon  : <FlightTakeoffIcon />,
            link  : "/pricecalculator",
        },

]