import React from 'react'
import ReactApexChart  from 'react-apexcharts';

function donut(dataset) {
    let angle = parseInt(dataset.value*3.6)
    if (dataset.value[0]<=1){
        angle = 4;
    }

    const state = {
        options : {
                labels: ["avg "+ dataset.name],

                chart: {
                        width: 300,
                        type: 'donut',
                        animations: {
                            enabled: true,
                            easing: 'easein',
                            speed: 800,
                            animateGradually: {
                                enabled: true,
                                delay: 1000
                            },
                            dynamicAnimation: {
                                enabled: true,
                                speed: 350
                            }
                        },
                        },    
                plotOptions: {
                        pie: {
                        startAngle: 0,
                        endAngle: angle,
                        donut: {
                            //  size: '65%',
                            labels: {
                              show: true,
                              name: {
                                show: true,
                                fontSize: '12px',
                                fontFamily: 'Helvetica, Arial, sans-serif',
                                fontWeight: 600,
                                color: dataset.color,
                                offsetY: 5,
                                formatter: function (val) {
                                  return val
                                }
                              },
                              value: {
                                show: true,
                                fontSize: '16px',
                                fontFamily: 'Helvetica, Arial, sans-serif',
                                fontWeight: 400,
                                color: undefined,
                                offsetY: 45,
                                formatter: function (val) {
                                  return val
                                }
                              },
                            },
                            }
                        }
                    },
                dataLabels: {
                        enabled: false
                    },
                fill: {
                        type: 'gradient',
                    },
                colors: dataset.color,
                legend:{
                        show:false
                    },
                responsive: [{
                                breakpoint: 480,
                                options: {
                                chart: {
                                    width: 200
                                },
                                legend: {
                                    show: false,
                                }
                                }
                    }]
                }
    }
    return (
        <>
            <ReactApexChart options={state.options} series={[dataset.value]} type="donut" height={120} width={200} />
        </>
    )
}

export default donut
