import time
import requests
import json 
class AWS:
 
	def __init__(self):
		self.init=""	
		self.location=""

	def get_spot_prices(self):
		proxies = {"http": "http://127.0.0.1:8888",
			  "https": "http://127.0.0.1:8888",
			}
		url = "http://website.spot.ec2.aws.a2z.com/spot.js"
		method = "get"

		headers = {
					 "Accept-Language": "en-US,en;q=0.9", 
					 "Accept-Encoding": "gzip, deflate, br", 
					 "Sec-Fetch-Site": "cross-site", 
					 "Sec-Fetch-Mode": "no-cors", 
					 "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36", 
					 "Referer": "https", 
					 "sec-ch-ua-mobile": "?0", 
					 "sec-ch-ua": "\"Chromium\";v=\"92\", \" Not A;Brand\";v=\"99\", \"Google Chrome\";v=\"92\"", 
					 "Sec-Fetch-Dest": "script"
					}
		params = {"callback": "callback", "_": str(int(time.time()*1000))}


		data = None

		response = requests.get(**{'url': url,'headers': headers, 'params': params, 'data': data , "proxies" : proxies})
		response = response.text.replace('callback(',"").replace(")","").replace(";","")

		return json.loads(response)

	def selfcall_get_on_demand_prices(self):
		proxies = {"http": "http://127.0.0.1:8888",
			  "https": "http://127.0.0.1:8888",
			}
		url = "http://b0.p.awsstatic.com/pricing/2.0/meteredUnitMaps/ec2/USD/current/ec2-ondemand-without-sec-sel/US%20East%20(Ohio)/Linux/index.json"
		method = "get"

		headers = {
						 "Accept-Language": "en-US,en;q=0.9", 
						 "Accept-Encoding": "gzip, deflate, br", 
						 "Sec-Fetch-Site": "cross-site", 
						 "Sec-Fetch-Mode": "no-cors", 
						 "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36", 
						 "Referer": "https", 
						 "sec-ch-ua-mobile": "?0", 
						 "sec-ch-ua": "\"Chromium\";v=\"92\", \" Not A;Brand\";v=\"99\", \"Google Chrome\";v=\"92\"", 
						 "Sec-Fetch-Dest": "script"
						}
		params = {"timestamp": str(int(time.time()*1000))}


		data = None

		response = requests.get(**{'url': url,'headers': headers, 'params': params, 'data': data})
		response = response.text.replace('callback(',"").replace(")","").replace(";","")

		return json.loads(response)

a = AWS()

json_result =  a.selfcall_get_on_demand_prices().get("regions")
# print json.dumps(json_result)

server_list = []
instance_with_server_list = []
instance_sizes = []
helper_data={}
if json_result.get('US East (Ohio'):
	for keys in json_result.get('US East (Ohio'):
		for item in ['t2','t3','t4','m3','m4','m5','m6',]:
			if item in keys:
				instance_sizes.append(
					json_result.get('US East (Ohio').get(keys)
					)			


print json.dumps(instance_sizes)
# util.writetoCSV("spot_prices.csv", [fields], prindata=False,mode="w+")
	
# for x in range(len(instance_sizes)):
# 	count=1
# 	for keys in range(len(helper_data.keys())):
# 		if float(helper_data.get(helper_data.keys()[keys])[x])==0:
# 			pass
# 			# print helper_data.get(helper_data.keys()[keys])[x]
# 		else:
# 			count+=1

# 	count = count-1

# 	avg = (float(helper_data.get(helper_data.keys()[0])[x])+float(helper_data.get(helper_data.keys()[1])[x])+float(helper_data.get(helper_data.keys()[2])[x])+float(helper_data.get(helper_data.keys()[3])[x])+float(helper_data.get(helper_data.keys()[4])[x]))/count

# 	util.writetoCSV("spot_prices.csv",[[instance_sizes[x],helper_data.get(helper_data.keys()[0])[x],helper_data.get(helper_data.keys()[1])[x],helper_data.get(helper_data.keys()[2])[x],helper_data.get(helper_data.keys()[3])[x],helper_data.get(helper_data.keys()[4])[x],avg,avg*24]],prindata=False) 
